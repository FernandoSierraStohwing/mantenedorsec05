
package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConexionDAO {
    //conexion con patron singleton
    private static Connection conn;
    String url = "jdbc:oracle:thin:@localhost:1521/xe";
    String usuario = "HR";
    String password = "hr";

    private ConexionDAO() throws SQLException {
        DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
        conn = DriverManager.getConnection(url, usuario, password);
    }

    public static Connection getConnection() throws SQLException {
        if (conn == null) {
            new ConexionDAO();
        }
        return conn;
    }
}
