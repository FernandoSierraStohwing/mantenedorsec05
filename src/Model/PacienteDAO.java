
package Model;

import Controller.Paciente;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author CETECOM
 */
public class PacienteDAO {
    //aca vamos a crear el mapping orm para que el paciente pueda ingresar sus datos
    //a la base de datos
    public boolean insertar(Paciente paciente){
        boolean resultado = false;
        String query = "INSERT INTO PACIENTE (RUT, NOMBRES, APELLIDO_UNO," + 
                "APELLIDO_DOS, CALLE, NUMERO, BLOCKK, DEPARTAMENTO, CIUDAD) VALUES (?,?,?,?,?,?,?,?,?)";
        
        try{
            PreparedStatement ps = ConexionDAO.getConnection().prepareStatement(query);
            
            int i = 1;
            ps.setString(i++, paciente.getRut());
            ps.setString(i++, paciente.getNombres());
            ps.setString(i++, paciente.getPrimerApellido());
            ps.setString(i++, paciente.getSegundoApellido());
            ps.setString(i++, paciente.getDireccion().getCalle());
            ps.setString(i++, paciente.getDireccion().getNumero());
            ps.setString(i++, paciente.getDireccion().getBlock());
            ps.setString(i++, paciente.getDireccion().getDepartamento());
            ps.setString(i++, paciente.getDireccion().getCiudad());
            
            resultado = true;
        }catch(SQLException ex){
            ex.printStackTrace(System.out);
        }
        return resultado;
    }
}
