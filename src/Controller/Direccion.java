
package Controller;

/**
 *
 * @author CETECOM
 */
public class Direccion {
    //atributos de direccion
    private String ciudad, calle, numero, departamento, block; 
    
    //constructores

    public Direccion(String ciudad, String calle, String numero, String departamento, String block) {
        this.ciudad = ciudad;
        this.calle = calle;
        this.numero = numero;
        this.departamento = departamento;
        this.block = block;
    }
    
    public Direccion() {
        this.ciudad = "";
        this.calle = "";
        this.numero = "";
        this.departamento = "";
        this.block = "";
    }
    
    //accesadores y mutadores

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }
    
    
    
}
