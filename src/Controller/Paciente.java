
package Controller;

import java.util.Date;

/**
 *
 * @author CETECOM
 */
public class Paciente {
    //atributos
    private String rut, nombres, primerApellido, segundoApellido;
    private Date fechaNacimiento;
    private Direccion direccion;
    
    //constructores

    public Paciente(String rut, String nombres, String primerApellido, String segundoApellido, Date fechaNacimiento, Direccion direccion) {
        this.rut = rut;
        this.nombres = nombres;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
    }
    
    public Paciente() {
        this.rut = "";
        this.nombres = "";
        this.primerApellido = "";
        this.segundoApellido = "";
        this.fechaNacimiento = new Date();
        this.direccion = new Direccion();
    }
    
    //accesadores y mutadores

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    
    public void mostrar(){
        System.out.println("**DATOS PERS.**\n" +
                           "RUT: " + this.rut + "\n" +   
                           "NOMBRES: " + this.nombres + "\n" +
                           "APELLIDOS: " + this.primerApellido + " " + this.segundoApellido + "\n" +
                                 
                           "**DIRECCION**\n" +
                           "CALLE: " + direccion.getCalle() + "\n"+
                           "NUMERO: " + direccion.getNumero()+ "\n" + 
                           "CIUDAD: " + direccion.getCiudad() + "\n"+ 
                           "BLOCK: " + direccion.getBlock() + "\n"+ 
                           "DEPARTAMENTO: " + direccion.getDepartamento());
    }
    
    
    
}
